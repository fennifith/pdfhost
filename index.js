const _express = require('express');
const _fs = require('fs');
const _app = _express();
const _exec = require('child_process').execSync;

let generation = 0;

_app.get('/file.pdf', (req, res) => {
    try { _fs.unlinkSync(__dirname + '/file.pdf'); } catch (e) {}
    _exec("pdflatex \"\\newcommand{\\generation}{" + (++generation) + "}\\input{file.tex}\"");

    let stat = _fs.statSync(__dirname + '/file.pdf');

    res.setHeader('Content-Length', stat.size);
    res.setHeader('Cache-Control', 'no-store');
    res.contentType('application/pdf');
    
    _fs.readFile(__dirname + '/file.pdf', (err, data) => {
        res.send(data);
    });
});

_app.listen(8081, '127.0.0.1');
